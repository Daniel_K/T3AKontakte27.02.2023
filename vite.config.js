import { fileURLToPath, URL } from 'node:url'
import { VitePWA } from 'vite-plugin-pwa'

import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    VitePWA({
      registerType: "autoUpdate",
      devOptions: {
        enabled: true
      },
      includeAssets: ["**/*.{png}"],
      manifest: {
        name: "PROJEKTNAME",
        start_url: "/PROJEKTNAME/",
        id: "/PROJEKTNAME/",
        short_name: "PROJEKTNAME",
        description: "Tolle Webapp",
        lang: "de",
        theme_color: "#031c36",
        icons: [
          {
            src: "/PROJEKTNAME/android-chrome-192x192.png",
            sizes: "192x192",
            type: "image/png"
          },
          {
            src: "/PROJEKTNAME/android-chrome-512x512.png",
            sizes: "512x512",
            type: "image/png"
          },
          {
            src: "/PROJEKTNAME/android-chrome-512x512.png",
            sizes: "512x512",
            type: "image/png",
            purpose: "any maskable"
          }
        ]
      }
    }),

  ],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url))
    }
  },
  base: '/T3AKontakte27.02.2023/'
})
