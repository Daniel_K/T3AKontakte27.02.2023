# kontakte2023

CRUD App with localStorage

Website: https://Daniel_K.codeberg.page/T3AKontakte27.02.2023

## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Compile and Minify for Production

```sh
npm run build
```

### Build and deploy to codeberg.pages
#### Linux
```sh
./deploy.sh
```

#### Windows
```sh
./deploy.cmd
```