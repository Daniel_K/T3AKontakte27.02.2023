import { ref } from 'vue';
import { defineStore } from 'pinia';
import { useKontakteStore } from './kontakte.js';

export const useModalUpdateStore = defineStore('modalUpdate', () => {
    const kontakte = useKontakteStore();
    // D A T A
    const isVisible = ref(false);

    const id = ref('-');
    const vorname = ref('-');
    const nachname = ref('-');
    const bot = ref(true);
    const gender = ref('w');
    const haarfarbe = ref('glatze');
    const schugroesse = ref('30');

    function show(kontakt) {
        // Daten anzeigen
        id.value = kontakt.id;
        vorname.value = kontakt.vorname;
        nachname.value = kontakt.nachname;
        bot.value = kontakt.bot;
        gender.value = kontakt.gender;
        haarfarbe.value = kontakt.haarfarbe;
        schugroesse.value = kontakt.schugroesse;
        

        // Fenster sichtbar machen
        isVisible.value = true;
    }

    function buttonCancelClick() {
        isVisible.value = false;
    }

    function buttonSaveClick() {
        // Bearbeiteten Kontakt speichern
        kontakte.updateKontakt({
            id: id.value,
            vorname: vorname.value,
            nachname: nachname.value,
            bot: bot.value,
            gender: gender.value,
            haarfarbe: haarfarbe.value,
            schugroesse: schugroesse.value
        });

        // Fenster unsichtbar machen
        isVisible.value = false;
    }

    return {
        isVisible,
        vorname,
        nachname,
        bot,
        gender,
        haarfarbe,
        schugroesse,
        show,
        buttonCancelClick,
        buttonSaveClick
    };
});