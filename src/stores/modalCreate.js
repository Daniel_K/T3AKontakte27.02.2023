import { ref } from 'vue';
import { defineStore } from 'pinia';
import { useKontakteStore } from './kontakte.js';

export const useModalCreateStore = defineStore('modalCreate', () => {
    const kontakte = useKontakteStore();
    // D A T A
    const isVisible = ref(false);

    const vorname = ref('-');
    const nachname = ref('-');
    const bot = ref(true);
    const gender = ref('w');
    const haarfarbe = ref('glatze');
    const schugroesse = ref(30);

    function show() {
        // Default-Werte setzen
        vorname.value = 'Max';
        nachname.value = 'Mustermann';
        bot.value = true;
        gender.value = 'w';
        haarfarbe.value = 'glatze';
        schugroesse.value = '30'

        // Fenster sichtbar machen
        isVisible.value = true;
    }

    function buttonCancelClick() {
        isVisible.value = false;
    }

    function buttonSaveClick() {
        // Neuen Kontakt speichern
        kontakte.createKontakt({
            vorname: vorname.value,
            nachname: nachname.value,
            bot: bot.value,
            gender: gender.value,
            haarfarbe: haarfarbe.value,
            schugroesse: schugroesse.value,
        });

        // Fenster unsichtbar machen
        isVisible.value = false;
    }

    return {
        isVisible,
        vorname,
        nachname,
        bot,
        gender,
        haarfarbe,
        schugroesse,
        show,
        buttonCancelClick,
        buttonSaveClick
    };
});