#!/usr/bin/env sh
# Bei Fehler sofort abbrechen
set -e

# dist-Verzeichnis komplett löschen
rm -rf dist

# build (dist-Verzeichnis erstellen)
npm run build

# komplett neues Repository in dist
# mit Branch 'pages' erstellen
cd dist
git init
git checkout -B pages
git add -A
git commit -m 'deploy'

# aktuelle Version hochladen
# alte Version wird gel ̈oscht
git push --force -u 'https://codeberg.org/Daniel_K/T3AKontakte27.02.2023.git' pages

# wieder in altes Verzeichnis springen
cd -